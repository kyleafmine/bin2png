use std::{convert::TryInto, path::Path};
use std::fs::File;
use std::io::BufWriter;


fn main() {
    let path = std::env::args().nth(1).expect("Yikers! Didnt put in a path did we?");
    if path == "decode" {
        extract();
        return;
    }
    let r = std::fs::read(path);

    if r.is_err() {
        panic!("Invalid file!")
    }
    let mut udata = r.unwrap();
    let mut data = Vec::with_capacity(udata.len());
    if std::env::args().nth(2).is_some() && std::env::args().nth(2).unwrap() == "--no-compress" {
        data = udata;
    } else {
        let mut compressor = bzip2::Compress::new(bzip2::Compression::best(), 30);
        compressor.compress_vec(&udata, &mut data, bzip2::Action::Run).unwrap();
    }
    let b = data.len();
    
    while (data.len() % 1600) != 0 {
        data.push(0 as u8);
    }
    data.resize(data.len() - 4, 0 as u8);
    let delta = (data.len() - b) as u32;
    //println!("{}", delta.to_be_bytes().len());
    for i in delta.to_be_bytes().iter() {
        data.push(i.clone());
    }
    println!("h={};d={}", data.len() / 1600, delta);
    let p = Path::new("output.png");
    let f = File::create(p).unwrap();
    let ref mut w = BufWriter::new(f);
    let mut encoder = png::Encoder::new(w, 400, (data.len() / 1600).try_into().unwrap());
    encoder.set_color(png::ColorType::RGBA);
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();
    writer.write_image_data(&data).unwrap();
}

fn extract() {
    let path = std::env::args().nth(2).expect("Yikers! Didnt put in a path did we?");
    let decoder = png::Decoder::new(File::open(path).unwrap());
    let (info, mut reader) = decoder.read_info().unwrap();
    let mut buf = vec![0; info.buffer_size()];
    reader.next_frame(&mut buf).unwrap();
    
    let delta = u16::from_le_bytes([buf[buf.len() - 1], buf[buf.len() - 2]]);
    println!("{}", delta);
    buf.resize(buf.len() - (delta + 4) as usize, 0);
    let mut obuf: Vec<u8> = Vec::with_capacity(buf.len());
    let mut decompressor = bzip2::Decompress::new(false);
    decompressor.decompress_vec(&buf, &mut obuf).unwrap();
    
    std::fs::write(std::env::args().nth(3).expect("Yikers! need 2 args"), obuf).unwrap();
}